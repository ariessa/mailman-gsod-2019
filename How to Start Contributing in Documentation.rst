==========================================
How to Start Contributing in Documentation
==========================================

In this guide, you will learn how to contribute to documentation. And yes, everyone can contribute to the documentation!

|

------------
How to Do It
------------

This guide assumes that you are familiar with reStructuredText and GitLab.

|

- **Choose a Platform**

  You can choose to either contribute on `GitLab <https://gitlab.com/mailman>`_ or our `community driven wiki <https://wiki.list.org/>`_. 
  The core documentation (on GitLab) and the wiki documentation (FAQ, HOWTOs) serve different purposes.
  The documentation on GitLab are updated more regularly than the ones at the community driven wiki.

|

- **Choose Type of Contribution**

  We really love newcomers! Therefore, new documentation is very welcomed. You can also update an existing documentation by fixing grammar, typos, structures or content.

|

- **Know the Documentation Styles**

  You are required to use American English with no contractions. Assume that the readers are not native English speakers. 
  Therefore, every documentation must be written using simple yet concise explanations. 
  Also, we prefer codes over videos and screenshots in documentation as the UIs are continuously being updated.

|

- **Start Writing**

  You can start writing new documentation by using the Documentation Template. For updating existing documentation, be sure to stick to the documentation styles and format.

|

- **Submit Your Work**
    
  + **GitLab**
  
    On the documentation page, click ‘Edit on GitLab’, fork the project and send a Merge Request.
    
  + **Community Driven Wiki**
  
    On the wiki, sign up for an account and request write permission for your user name by sending a note to the `Mailman Steering Committee <mailman-cabal@python.org>`_.


|

Congratulations, you have just contributed to our documentation!

|

------
Result
------
A contribution to Mailman’s documentation
