.. role:: bash(code)
   :language: bash

.. TO DO
   Change the words 'Documentation Template' below to the document
   title.  It is a good idea to keep it to less than 10 words!
   Don't touch the stars (*)!  They are part of the reStructuredText
   document formatting system.

Documentation Template
======================

|

.. TO DO
   'Introduction' is a good generic name for this section, but if you
   have a good, short title which is more specific, that is often
   useful to let the reader know what the section is about.

**Introduction**

.. TO DO
   Explain the purpose of the documentation, and the target audience.
   'Purpose' means *what* you intend to explain, and 'audience' means
   who it is useful to.  For example, an API document is targeted to
   *developers*, while the UI documentation addresses *users*.
   Another audience distinction that is often useful is *beginner*
   vs. *expert* levels.  The example text is a bit verbose.  Often
   about 2 sentences will suffice.

Open this file in a plain text editor.

Then, you may use this template to create an initial draft of Mailman
documentation by changing the visible contents as suggested in the
comments.  The comments are most useful to beginning tech writers, and
the template itself is a useful reminder to tech writers of all levels
of experience with this project.


|

.. TO DO
   'Pre Content' is a good generic name for this section, but if you
   have a good, short title which is more specific, that is often
   useful to let the reader know what the section is about.
   For example, if you are writing a 'How to' guide, a good name for this section would be 'How to Do It'.

**Pre Content**


.. TO DO
    Describe the prerequisites of the documentation in less than 2 sentences.
    'Prerequisites' means what the readers need to have or know before reading the documentation.
    For example, the prerequisites for Mailman 3 Installation guide would be knowledge in using Linux.
    You can remove this section if there is no prerequisites.
 
This guide assumes that you are familiar with reStructuredText and GitLab.


|

**Content**

.. TO DO
    Provide simple yet concise explanations about the content of documentation.
    What should the audience know? Any extra technical information?
    If there is an existing documentation, summarise it and provide the link to the documentation.
    Explain as if you’re talking directly to the audience. Use bullet points and numbering over paragraphs.
    If necessary, provide codes, videos or screenshots.
    
Before writing, you should know that Mailman's documentation on GitLab are written using reStructuredText. 
If you are familiar with Markdown syntax, then this should be easy to pick up.
You can use `Quickstart reStructuredText <https://gitlab.com/ariessa/mailman-gsod-2019/blob/master/Quickstart%20reStructuredText.rst>`_ as a quick syntax guide.


|

**Post Content**


.. TO DO
    Add 1-2 sentences to explain what will the reader accomplish by reading the documentation.
    What will the reader accomplish by reading the documentation?
    For example, the reader will learn how to write a documentation using template.
    
Congratulations, you just learned about Mailman's Documentation Template!


|

**Result**


.. TO DO
    Add 1-2 sentences describing the expected result of reading or following the documentation.
    For example, the reader will produce an initial draft of Mailman documentation.
    

Initial draft of Mailman documentation.