.. role:: bash(code)
   :language: bash

========================================== 
How to Migrate from Mailman 2 to Mailman 3
==========================================

In this guide, you will be using the command line to perform migration from Mailman 2 to Mailman 3. 
Before following this guide, you must approve all subscription requests and force send all emails in digest mailbox.

|

------------
How to Do It
------------

This guide assumes that you have either use Mailman or have been using Mailman 2, with Pipermail as the archiver. For advices on other types of installation, please post to `Mailman 3 Users <mailto:mailman-users@mailman3>`_.

The following list will be used as an example: *foo-list@example.com*

|

* **Install Mailman 3**

  .. note:: You need Python 3 to install Mailman 3 into your system. If you have installed both Python 2 and Python 3, you should follow this guide by replacing :bash:`$ python` with :bash:`$ python3`. If you are unsure which Python version is installed in your system, you can check using :bash:`$ python -V`.


  You can refer to this `documentation <https://mailman.readthedocs.io/en/latest/src/mailman/docs/install.html>`_ for installation. 
  For users who installed Mailman using the system packaging system, you can find the components of Mailman 3 at /var/lib/mailman.
  The root of mailing list archives can be found at /var/lib/mailman/archives.
  Since Mailman is not installed in the ordinary user's path, privileged access is needed to configure mailing lists.
  During migration, it is advisable for you to stop both Mailman 2 and Mailman 3 instances.
    

|

* **Migrate the list configuration from Mailman 2 to Mailman 3**
  
  .. note:: If your Mailman 2 list is of version 2.1 or less, its LISTNAME.mbox file is probably in good shape. Even so, all mailboxes should be checked for defects before importing. This is because certain defects such as missing Message-ID headers or unparseable Date headers will be corrected or ignored by the import process. Another defect that will surely cause problems is lines beginning with the word 'From' in message bodies. These lines will be seen as the start of a new message. In Mailman 2, there is a script at $prefix/bin/cleanarch that can identify and fix most of the aforementioned defects. Even so, it is not perfect. Cases have been observed where a post includes in its body a copy of other message including the From separator. This will normally occur only on an old list which includes spam messages or other email problems in its subject matter, but it is something to be aware of.

  ::

    $ mailman import21 foo-list@example.com /path/to/mailman2/foo-list/config.pck
    
|

* **Migrate the list archives from Mailman 2 to Mailman 3**

    
  .. note:: $var_prefix is an installation dependent directory which is usually at /usr/local/mailman or /var/lib/mailman.


  ::

    $ python manage.py hyperkitty_import -l foo-list@example.com $var_prefix/archives/private/foo-list.mbox/foo-list.mbox

|

* **Rebuild the index for list**


  ::

    $ python manage.py update_index_one_list foo-list@example.com

|

* **Delete Mailman 2 list**

    
  .. note:: $prefix is an installation dependent directory which is usually at /usr/local/mailman or /usr/lib/mailman 


  ::

    $ $prefix/bin/rmlist foo-list

|

Well done, you have just migrated a list from Mailman 2 to Mailman 3!


| 
 
------
Result
------

A list in Mailman 3


|


------------------------
Optional Migration Steps
------------------------

Now that you have successfully migrated a list, you may want to follow the following steps:



- Add notice to list archives by editing index.html at the root of your mailing list archives to keep your old archives .
    
- Add a redirect at old list page to automatically redirect users to Mailman3 list-info page