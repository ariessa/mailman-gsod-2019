.. role:: bash(code)
   :language: bash


===========================
Quickstart reStructuredText
===========================

This guide provides a quick syntax reference to writing documentation in reStructuredText. 

|


-----------------
Table of Contents
-----------------

+ `Headings`_
+ `Text Formatting`_
+ `Indentation`_
+ `New Line`_
+ `Lists`_
+ `Links`_
+ `Inline Codes`_
+ `Code Blocks`_
+ `Tables`_
+ `Images`_
+ `Boxes`_
+ `Comments`_


|


********
Headings
********

Headings are used as titles, sections, and subsections in documentation. You can choose to use any of the following: = - * # ^ “

Even so, it is better to stick to the convention below for standardization across documentation.

::

    =======
    Heading 1
    =======
    
    -------------
    Heading 2
    -------------
    
    ***********
    Heading 3
    ***********
    
    ########
    Heading 4
    ########
    
    ^^^^^^^^^^^^^
    Heading 5
    ^^^^^^^^^^^^^
    
    “““““““““““““
    Heading 6
    “““““““““““““

|

***************
Text Formatting
***************

Text formatting is used to give emphasis on words or phrases in documentation.

::

    *italic*
    **bold**

|

***********
Indentation
***********

Indentation is used for block quotes, definition, and nested content. Indentation can be created using tabs.

::

    This is a paragraph.
        This is a nested content inside paragraph.

|

********
New Line
********

New line is used to improve readability of documentation. You must include a blank line before and after the vertical bar, |

::

    This is a sentence.

    |

    This is another sentence.

|

*****
Lists
*****

Lists are used to represent bullet list items, numbered list items, and definition list items. For bullet list items, you can choose to use any of the following: + - *

::

    1. This is a numbered list item.
    2. This is a numbered list item.

    + This is a bullet list item.
    + This is a bullet list item.

    Term 1
        This is the definition of term 1.
    Term 2
        This is the definition of term 2.

|

*****
Links
*****

Links are used to take users to another website, documentation, or application. You can also use links to direct users to videos on online streaming platforms.

::

    `<https://list.org/>`_
    `GNU Mailman <https://list.org/>`_
    

|

************
Inline Codes
************

Inline Codes are used to differentiate between short commands and text in a sentence.

::

    .. role:: bash(code)
   :language: bash

   :bash:`$ python manage.py update_index_one_list foo-list@example.com`


|

***********
Code Blocks
***********

Code blocks are usually included to give users an explicit approach in performing a series of actions. You must include a blank line after double colon.

::

    ::
    
        sudo apt-get update
    

|

******
Tables
******

Tables are used to represent information in tabular format.

::

    +------------+-----------+-------------------+
    | Header 1   | Header 2  | Header 3          |
    +============+===========+===================+
    | Row 1      | Column 2  | Column 3          |
    +------------+-----------+-------------------+
    | Row 2      | Merged Column 2 and Column 3  |
    +------------+-----------+-------------------+
    | Row 3      | Merged    | - List item 1     |
    +------------+ Row 3 and | - List item 2     |
    | Row 4      | Row 4     | - List item 3     |
    +------------+-----------+-------------------+
    
|


******
Images
******

Images are used to represent information that cannot be shown using textual format.

::

    .. image:: mailman.jpg
        :width: 300px
        :align: center
        :height: 300px
        :alt: this is an alternate text
        
        
|


*****
Boxes
*****

Boxes are used to inform or give out information to users.

::

    .. note:: This is a note box.

    .. warning:: This is a warning box.


|


********
Comments
********

Comments are used to write text that should not be rendered to end-users.

::

    .. This is a comment