# Mailman-GSOD-2019

Project Title: Instructions for Migration from Mailman 2 to Mailman 3


<br/>

## Documentation

* [What You Should Know Before Migration](What You Should Know Before Migration.rst)
* [How to Migrate from Mailman 2 to Mailman 3](How to Migrate from Mailman 2 to Mailman 3.rst)
* [How to Start Contributing in Documentation](How to Start Contributing in Documentation.rst)
* [Documentation Template](Documentation Template.rst)
* [Quickstart reStructuredText](Quickstart reStructuredText.rst)