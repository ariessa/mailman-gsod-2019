=====================================
What You Should Know Before Migration
=====================================

Mailman 3 is the new version that is being actively developed. 
On the other hand, Mailman 2 is currently in maintenance mode and will not receive any feature updates.

|

Well, here are some of the reasons why you should upgrade to Mailman 3:

+ A well tested and rewritten code base
+ Support for REST API (if you need to integrate with other services)
+ Support for multiple domains in same installation (without listname collisions)
+ A real database backend for settings and configuration
+ Modern Web UI for list administration and subscription management
+ Support for social logins (and possibly LDAP/openid integration)
+ Much improved and interactive archiver / forum with ability to reply from within the archiver

|

After knowing why you should upgrade to Mailman 3, you probably wondering on what else you should know before migration. 

|

* **Broken URLs for Archived Migration**

  If you are planning to migrate from the bundled Pipermail archive manager in Mailman 2 to the HyperKitty
  archive manager designed for Mailman 3, you should know that all archived messages will be imported with new URLs, thus breaking the old URLs. For more information, you can refer to our GitLab's issue on `supporting legacy private archives <https://gitlab.com/mailman/hyperkitty/issues/267>`_.
  If you still want to use the old URLs for archived messages, you can keep the web server configuration and HTML files generated for the archives intact. You can do this by putting up a notice to viewers that it is now a read-only archive. 
  
  `Example <https://mail.python.org/pipermail/security-sig/>`_


  .. note:: The above mechanism won’t work for Pipermail private archives since the archives are gated with a password. Without a Mailman 2 list, there is no password. However, you can still import them to Mailman 3. 

|

* **Unavailability of Some Configurations and Settings**

  There are some configurations and settings from Mailman 2 that has not been implemented in Mailman 3’s user interface (UI). Even though those settings will be migrated to Mailman 3, you may not be able to change them from the Web UI. But, all of those settings should be exposed in the UI very soon.

|

* **No Support for Bounce Processing**

  Currently, Mailman 3 does not support bounce processing. You can subscribe to `Mailman 3 Users <mailman-users@mailman3.org>`_ to get the latest updates.
